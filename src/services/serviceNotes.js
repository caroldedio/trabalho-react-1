const result = [
    { text: 'Lavar o carro', id: new Date().getTime() + 15 },
    { text: 'Dar vacina na Alice', id: new Date().getTime() - 99 }
]

const listNotes = () => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result)
        }, 2)
    })
}


const saveNotes = (text, index) => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (index >= 0) {
                result[index].text = text
                resolve()
            } else {
                result.unshift({
                    text: text,
                    id: new Date().getTime()
                })
                resolve()
            }
        }, 2)
    })
}


module.exports = {
    listNotes,
    saveNotes
}