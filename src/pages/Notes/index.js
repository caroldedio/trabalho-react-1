import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    text: '',
    results: []
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text, this.state.index)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: ''
    })
  }

  editNote = async (index) => {
    if(index !== -1) {
      this.setState({
        text: this.state.notes[index].text,
        new: true,
        index: index
      })
    }
  }

 
  delNote = (index) => {
    let notes = this.state.notes;
    notes.splice(index, 1);
    this.setState({
      notes: notes
    });
  }

  handleKey = async (event) =>{
    if (event.key === 'Enter'){
      this.setState ({
        loadingSave: true
      })
      await saveNotes(this.state.text)
      this.setState({
        notes: await listNotes(),
        loadingSave: false,
        new: false,
        text: ''
      })
    }
  }

  handleInputChange = async (busca) =>{
    this.setState({
      results: this.state.notes.filter(nota=> nota.text.toUpperCase().search(busca.target.value.toUpperCase()) !==-1 )
    })
  }


  
  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>
              <input type='search' onChange={this.handleInputChange} placeholder='Procurar...' />
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

      
          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }

            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }


            {this.state.new && (
              <div className='item'>
                <input value={this.state.text} onChange={this.editText} onKeyPress={this.handleKey} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button type="submit" onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}
            
            {this.state.notes.map((note, index) => (
              <div key={note.id} className='item'>
                <input disabled onChange={this.editText} className='left' type='text' value={note.text} placeholder='Digite sua anotação' />
                <div className='right'>
                  { !note.id && (<button onClick={this.saveNote}>Salvar</button>) }
                  { note.id && (<button onClick={() => this.delNote(index)}>Excluir</button>) }
                  { note.id && (<button onClick={() => this.editNote(index)}>Editar</button>) }
                </div>
              </div>
            ))}

          </div>
        </div>
      </div>
    )
  }
}
